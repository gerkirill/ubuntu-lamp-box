# Vagrant-бокс со свежей Ubuntu и LAMP на борту

## Особенности

* PhpMyAdmin доступен по URL /phpmyadmin, доступ - root : vagrant
* доступ по ssh: логин и пароль - vagrant
* для папки /etc создан локальный git-репозиторий, чтобы отслеживать изменения конфигурации, например при обновлении системы
* установлены наиболее востребованные расширения для php
* phpMyAdmin доступен по URL /phpmyadmin

## Пример Vagrantfile 

Нужно заменить 'pbpn' в 2х местах на нужное имя домена

    Vagrant.configure("2") do |config|
        config.vm.box = "ubuntu-lamp-2014-05-16"
        config.vm.box_url = "https://bitbucket.org/gerkirill/ubuntu-lamp-box/downloads/package-2014-05-16.box"
        config.vm.network :private_network, ip: "192.168.54.10"
        config.vm.hostname = "pbpn.local"

        config.vm.provider "virtualbox" do |v|
            v.name = "pbpn"
            v.customize ["modifyvm", :id, "--memory", "1000"]
            v.customize ["modifyvm", :id, "--cpus", "1"]
            v.customize ["modifyvm", :id, "--ioapic", "on"]
        end
    end
    
Приведенный выше фрагмент устанавливает base-box с php 5.3 на борту. Для того чтобы получить LAMP с php 5.5 нужно внести следующие изменения:

        config.vm.box = "ubuntu-lamp-2014-07-07"
        config.vm.box_url = "https://www.dropbox.com/s/r7awn51cpfef8gz/package-2014-07-07-php55.box?dl=1"
        
В данном вагрант-боксе вэбрут Apache настроен так, что совпадает с корневой папкой проекта (той в которой расположен Vagrantfile). Если нужно чтобы 
вэбрут указывал на подпапку проекта, например "web" нужно изменить вагрант-файл вот так:

    $script = <<SCRIPT
        echo Adjusting webroot symlink...
        rm -rf /var/www
        ln -s /vagrant/web /var/www
        service apache2 restart
    SCRIPT
    
    Vagrant.configure("2") do |config|
        config.vm.provision "shell", inline: $script
        ...